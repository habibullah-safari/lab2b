
import java.util.Random;
import java.util.Scanner;

public class Wordle {
    public static String generateWord() {
        String[] arrayOfWords = {"LIGHT", "MIGHT", "WHITE", "FLAKE", "TRAIN", "NOISE", "VOICE", "VOCAL",
            "ANGER", "CHASE", "CABIN", "RIGHT", "FOCAL", "FOCUS", "TRAIN", "WASTE", "EIGHT", "YACHT", "ADOPT", "CRATE"};
        Random randomWord = new Random();
        int n = randomWord.nextInt(arrayOfWords.length);
        String convertedInt = arrayOfWords[n];
        return convertedInt;
    }
    /* generateWord method takes no parameters and it contains an array of 20 words 5 letters long. using the random class
     it chooses an integer and returns the word from that position in array.
     */
    public static boolean letterInWord(String word, char letter) {
        for (int index = 0; index < word.length(); index++) {
            if (word.charAt(index) == letter) {
                return true;
            }
        }
        return false;
    }
    /* letterInWord method takes a String and a Char as parameters. After determining if the String 
    contains that Char or not it return a boolean accordingly.
     */
    public static boolean letterInSlot(String word, char letter, int position) {
        for (int index = 0; index < word.length(); index++) {
            if (position > word.length()) {
                System.out.println("Position out of range");
                return false;
            } else if (letter == word.charAt(position)) {
                return true;
            }
        }
        return false;
    }
    /*letterInSlot method takes a String word, a Char letter and a int position of the Char in the String.
    it check for the exact position of the Char in the String and returns a boolean accordingly.
     */
    public static String[] guessWord(String answer, String guess) {
        String[] colorsArray = new String[answer.length()];
        int index = 0;
        while (index < guess.length()) {
            char character = guess.charAt(index);
            if (letterInSlot(answer, character, index)) {
                colorsArray[index] = "Green";
            } else if (letterInWord(answer, character)) {
                colorsArray[index] = "Yellow";
            } else {
                colorsArray[index] = "White";
            }
            index++;
        }
        return colorsArray;
    }
    /* guessWord Method takes two Strings one being the randomly generated word and another
    representing the user's answer. it calls the letterInSlot method to check for the exact position
    of the guessed word and it also calls the letterInWord method to check if the guessed word has the
    answer's letters in it at all or not.then, in creates an array of Strings indecating 3 colors and returns it.
     Green for the exact position of the Char in word, yellow for existence of Char in String and White 
     for String not containing the Char at all. 
     */
    public static void presentResults(String word, String[] colors) {
        String color = "";
        for (int index = 0; index < word.length(); index++) {
            if (colors[index].equals("Green")) {
                color += "\u001B[32m" + word.charAt(index) + "\u001B[0m";
            } else if (colors[index].equals("Yellow")) {
                color += "\u001B[33m" + word.charAt(index) + "\u001B[0m";
            } else {
                color += "\u001B[0m" + word.charAt(index);
            }
        }
        System.out.println(color);
    }
    /* presentResults method takes a String of 5 letters and an array of colors. 
    and it prints out the Chars of the String according to the colors indicated in 
    the array of colors and prints it out.
     */
    public static String readGuess() {
        System.out.println(" Please enter your 5 letters long guessed word");
        Scanner reader = new Scanner(System.in);
        String upperCased = "";
        for (int index = 0; index <= 5; index++) {

            String userInput = reader.next();
            if (userInput.length() == 5) {
                upperCased = userInput.toUpperCase();
                return upperCased;
            } else {
                System.out.println("Please enter a valid guess");
            }
        }
        return upperCased;
    }
    /* readGuess method takes no parameters, it ask the user to enter their guessed word.
    then, it makes sure that the entered word is 5 letters exactly and afterwards it changes
    the guessed word to upperCase letters and returens it. 
     */
    public static void runGame(String word) {
	    System.out.println("Welcome to Wordle");
        int chances = 6;
        for (int index = 0; index < 6; index++) {
            String guessedWord = (readGuess());
            String[] arrayOfColors = guessWord(word, guessedWord);

            presentResults(guessedWord, arrayOfColors);

            boolean isGreen = arrayOfColors[0].equals("Green") && arrayOfColors[1].equals("Green")
                    && arrayOfColors[2].equals("Green") && arrayOfColors[3].equals("Green") && arrayOfColors[4].equals("Green");
            if (isGreen) {
                System.out.print("Congrats you won");
                return;
            }
            chances--;
            System.out.println(" You have " + chances + " chances left ");
        }
        System.out.println("Sorry you lost Try again later");
    }
    /* runGame method takes a String as parameter given by the main method.
     it gives the users 6 chances. calls the readGuess to takes user input(capitalized).
     then passes the input to guessWord along with the random word. recieves an array of colors fro m
     guessWord. then passes the array of colors with the users guess to presentResults method. 
     it congratulates the user if the guessed word matches exactly the random word (answer).
     it says sorry if the user can not guess the word correctly in 6 tries.
     */
}
