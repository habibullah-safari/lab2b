
import java.util.Scanner;

public class WordGuesser {
    //First Method
    public static int isLetterInWord(String word, char character) {
        int Counter = 0;
        while (Counter < word.length()) {
            if (word.charAt(Counter) == character) {
                return Counter + 1;
            }
            Counter++;
        }
        return -1;
        /**
         * this method takes a String and a character then uses the while loop
         * to checks the String one letter at a time if the characater exists in
         * the string if it exists it returns its position otherwise it returns
         * -1.
         */
    }

    //Second Method. 
    public static void printWord(String givenWord, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {

        boolean[] booleanArray = {letter0, letter1, letter2, letter3};
        int number = 0;
        String result = "Your result so far is ";
        while (number < givenWord.length()) {
            if (booleanArray[0] && number == 0) {
                result += givenWord.charAt(0);
            } else if (booleanArray[1] && number == 1) {
                result += givenWord.charAt(1);
            } else if (booleanArray[2] && number == 2) {
                result += givenWord.charAt(2);
            } else if (booleanArray[3] && number == 3) {
                result += givenWord.charAt(3);
            } else {
                result += " _ ";
            }
            number++;
        }
        System.out.println(result);
    }

    /*printWord method takes a word and for booleans and after comparing it returns the values whose positions are the same */
    //Third Method. 
    public static void runGame(String execute) {
		System.out.println("Welcome to Hangman game");
		
        boolean boolGuess1 = false;
        boolean boolGuess2 = false;
        boolean boolGuess3 = false;
        boolean boolGuess4 = false;
        //all booleans are set to false at first

        int missCount = 0;
        final int missLimit = 6;

        boolean lettersNotGuessed = (boolGuess1) && (boolGuess2) && (boolGuess3) && (boolGuess4);
        /**
         * boolean letterNot Guessed is true while all 4 other booleans are
         * false the following loop runs until the booleans have the value true
         * or the limit is reached.
         */

        while (!lettersNotGuessed || missCount <= missLimit) {
            System.out.println(" Please enter a letter you guessed ");
            Scanner reader = new Scanner(System.in);
            String inputString = reader.next();
            char inputLetter = inputString.charAt(0);

            //takes the letter as String but then stores it as Char because there is no Reader = nextCahr();
            //calls IsLetterInWord method and passes the String and Char to this method.
            int returnedInt = isLetterInWord(execute, inputLetter);

            /* Stores the returned value to be used in if statement */
            if (returnedInt == 1) {
                boolGuess1 = true;
                printWord(execute, boolGuess1, boolGuess2, boolGuess3, boolGuess4); //calls the printWord mothod to the status of the game
            } else if (returnedInt == 2) {
                boolGuess2 = true;
                printWord(execute, boolGuess1, boolGuess2, boolGuess3, boolGuess4); //calls printWord...
            } else if (returnedInt == 3) {
                boolGuess3 = true;
                printWord(execute, boolGuess1, boolGuess2, boolGuess3, boolGuess4); //calls printWord...
            } else if (returnedInt == 4) {
                boolGuess4 = true;
                printWord(execute, boolGuess1, boolGuess2, boolGuess3, boolGuess4); //calls printWord...
            } else {
                System.out.println(" Good guess ! but try again ");
                missCount++;
                printWord(execute, boolGuess1, boolGuess2, boolGuess3, boolGuess4); //calls printWord...
            }
            boolean allLettersGuessed = (boolGuess1 == true) && (boolGuess2 == true) && (boolGuess3 == true) && (boolGuess4 == true);
            if (allLettersGuessed) {
                System.out.println("Congratulation you won !!!");
                return; // stops the loop if all four booleans are set to true. "letters are guessed correctly"
            }

        }
        boolean lostGame = (boolGuess1 == false) || (boolGuess2 == false) || (boolGuess3 == false) || (boolGuess4 == false);
        if (lostGame) {
            System.out.println("Sorry you lost");
        }
    }
}
