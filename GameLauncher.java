import java.util.Scanner;
public class GameLauncher{
	public static void main(String []args){
		/* This method greets the user and based on there choice initiates the chosen game
		*/
		Scanner reader = new Scanner(System.in);
		System.out.println(" Welcome to Game launcher Press 1 to play Hangman or 2 to play Wordle");
		int choice = reader.nextInt();
		
		//this loop makes sure that the user enteres 1 or 2.
		int chance = 0;
		while(chance<5){
		if (choice==1){
			playHangman();
		}else if (choice == 2){
			playWordle();
		}else{
			System.out.println(" Welcome to Game launcher Press 1 to play Hangman or 2 to play Wordle");
		} reader.next();
		chance++;
		}
	}
	public static void playHangman(){
		//calls runGame from WordGuesser class
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to Hangman game");
		System.out.println(" Please Enter a 4 letter word without repetitive letters ");
        String firstInputWord = reader.next();
        WordGuesser.runGame(firstInputWord);
	}
		// calls rungame from class Wordle 
	public static void playWordle(){
        String generated = Wordle.generateWord();
        Wordle.runGame(generated);
    }
}